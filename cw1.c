//F28HS C Coursework
//Author - Rory Dobson, rmd5

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//Structure to hold rgb values of individual pixels
struct pixel{
    int red, green, blue;
} pixel;

//Structure to hold the PPM information
struct PPM {
    int width, height, max;
    struct pixel ** pixel;
} PPM;

struct PPM * getPPM(FILE * f){

    //String to store lines of PPM file, which are no longer than 70 characters
    char buffer[70];
    struct PPM * im = malloc(sizeof(struct PPM));
    fgets(buffer, 70, f);

    //Verify file is in P3 format
    //P3 appears at the top of the file so it's easy to check
    if(strcmp(buffer, "P3\n") != 0){
        printf("File must be in P3 format");
        exit(0);
    }

    //Read next line
    fgets(buffer, 70, f);

    //While the line starts with a # move to the next line
    while(buffer[0] == '#'){
        fgets(buffer, 70, f);
    }

    //Read the width and height of the image, drop a line, then read the max colour value
    sscanf(buffer, "%d %d", &im->width, &im->height);
    fgets(buffer, 70, f);
    sscanf(buffer, "%d", &im->max);

    //Allocate space for the pixels by first allocating space for the height and width and looping through
    im->pixel = malloc(sizeof(im->height));
    int i, j;
    for(i = 0; i < im->height; i++){
        im->pixel[i] = malloc(sizeof(im->width));
        for(j = 0; j < im->width; j++){
            fscanf(f, "%d %d %d", &(im->pixel[i][j].red), &(im->pixel[i][j].green), &(im->pixel[i][j].blue));
        }
    }

    return im;
}

void showPPM(struct PPM * im){
    printf("P3\n");
    printf("Width:  %d\n", im->width);
    printf("Height: %d\n", im->height);
    printf("Max:    %d\n", im->max);

    //Print out the pixel information with formatting alterations
    int i,j;
    int l = 0;
    for(i = 0; i < im->height; i++){
        for(j = 0; j < im->width; j++){
            struct pixel p = im->pixel[i][j];
            l++;
            printf("r %d: %d  g %d: %d  b %d: %i\n", l, p.red, l, p.green, l, p.blue);
        }
    }
}

struct PPM * encode(struct PPM * im, char * message, unsigned int mSize, unsigned int secret){

    //Create random seed using secret key
    srand(secret);
    int i;

    //Convert message to binary
    int l;
    char * binary = malloc(mSize*8);
    binary[0] = '\0';
    for(l = 0; l < mSize; l++){
        char ch = message[l];
        int m;
        for(m = 7; m >= 0; m--){
            if(ch & (1 << m)){
                strcat(binary,"1");
            } else {
                strcat(binary,"0");
            }
        }
    }

    printf("\n%s\n", binary);

    //Loop through entire message
    for (i = 0; i < mSize*8; i += 3){
        int j, k;
        j = rand() % im->width; //Create random number ensuring it's not bigger than the width of the image
        k = rand() % im->height; //Create random number ensuring it's not bigger than the height of the image

        struct pixel * p = &(im->pixel[j][k]);

        //Convert the red value to binary
        char * rBinary = malloc(8);
        rBinary[0] = '\0';
        char r = p->red;
        int rLoop, x;
        for(rLoop = 7; rLoop >= 0; rLoop--){
            x = r >> rLoop;
            if(x & 1){
                strcat(rBinary,"1");
            } else {
                strcat(rBinary,"0");
            }
        }

        //Convert the green value to binary
        char * gBinary = malloc(8);
        gBinary[0] = '\0';
        char g = p->green;
        int gLoop;
        for(gLoop = 7; gLoop >= 0; gLoop--){
            x = g >> gLoop;
            if(x & 1){
                strcat(gBinary,"1");
            } else {
                strcat(gBinary,"0");
            }
        }

        //Convert the blue value to binary
        char * bBinary = malloc(8);
        bBinary[0] = '\0';
        char b = p->blue;
        int bLoop;
        for(bLoop = 7; bLoop >= 0; bLoop--){
            x = b >> bLoop;
            if(x & 1){
                strcat(bBinary,"1");
            } else {
                strcat(bBinary,"0");
            }
        }

        if((mSize*8 - i) >= 3){
            //set the last digit of the red binary to the next value of the message
            printf("\nOriginal red at p[%d][%d]:   %s    ", j, k, rBinary);
            rBinary[7] = binary[i];
            printf("New red: %s", rBinary);

            //Convert the red binary back into an integer and save it back into the original pixel
            p->red = binaryToInteger(atoi(rBinary));
            printf("   New value: %d", p->red);

            //set the last digit of the green binary to the next value of the message
            printf("\nOriginal green at p[%d][%d]: %s  ", j, k, gBinary);
            gBinary[7] = binary[i + 1];
            printf("New green: %s", gBinary);

            //Convert the green binary back into an integer and save it back into the original pixel
            p->green = binaryToInteger(atoi(gBinary));
            printf("   New value: %d", p->green);

            //set the last digit of the blue binary to the next value of the message
            printf("\nOriginal blue at p[%d][%d]:  %s   ", j, k, bBinary);
            bBinary[7] = binary[i + 2];
            printf("New blue: %s", bBinary);

            //Convert the blue binary back into an integer and save it back into the original pixel
            p->blue = binaryToInteger(atoi(bBinary));
            printf("   New value: %d\n", p->blue);
        } else if((mSize*8 - i) == 2){
            //Repeat steps above if there's two remaining numbers in the message binary
            printf("\nOriginal red at p[%d][%d]:   %s    ", j, k, rBinary);
            rBinary[7] = binary[i];
            printf("New red: %s", rBinary);

            p->red = binaryToInteger(atoi(rBinary));
            printf("   New value: %d", p->red);

            printf("\nOriginal green at p[%d][%d]: %s  ", j, k, gBinary);
            gBinary[7] = binary[i + 1];
            printf("New green: %s", gBinary);

            p->green = binaryToInteger(atoi(gBinary));
            printf("   New value: %d\n", p->green);
        } else if((mSize*8 - i) == 1){
            //Repeat the steps above if there is only one remaining number in the message binary
            printf("\nOriginal red at p[%d][%d]:   %s    ", j, k, rBinary);
            rBinary[7] = binary[i];
            printf("New red: %s", rBinary);

            p->red = binaryToInteger(atoi(rBinary));
            printf("   New value: %d\n", p->red);
        }
    }
    return im;
}

//Method to convert binary input into a decimal output
int binaryToInteger(int binary){
    int integer = binary;
    int decimal = 0;
    int base = 1;
    int remainder;
    while(integer > 0){
        remainder = integer % 10;
        decimal = decimal + remainder * base;
        integer = integer / 10 ;
        base = base * 2;
    }
    return decimal;
}

void writeFile(struct PPM * alt){
    FILE * fout = fopen("alt.ppm", "w");
    fprintf(fout, "P3\n%d %d\n255\n", alt->width, alt->height);

    int i,j;
    for(i = 0; i < alt->height; i++){
        for(j = 0; j < alt->width; j++){
            struct pixel p = alt->pixel[i][j];
            fprintf(fout, "%i %i %i\n", p.red, p.green, p.blue);
        }
    }
    fclose(fout);
}

char * decode(struct PPM * im, unsigned int secret) {
    srand(secret);
    int i;
    char * binary = malloc(800);//Max length of message binary
    char * message = malloc(100);//Max length of message

    //Loop through the max size of the message
    //800/3 because we're pulling 3 bits out at a time, could also increment the loop in 3s
    for (i = 0; i < 800/3; i++){
        int j, k;
        j = rand() % im->width; //Create random number ensuring it's not bigger than the width of the image
        k = rand() % im->height; //Create random number ensuring it's not bigger than the height of the image

        struct pixel * p = &(im->pixel[j][k]);

        //Convert the red value to binary
        char * rBinary = malloc(8);
        rBinary[0] = '\0';
        char r = p->red;
        int rLoop, x;
        for(rLoop = 7; rLoop >= 0; rLoop--){
            x = r >> rLoop;
            if(x & 1){
                strcat(rBinary,"1");
            } else {
                strcat(rBinary,"0");
            }
        }

        //Convert the green value to binary
        char * gBinary = malloc(8);
        gBinary[0] = '\0';
        char g = p->green;
        int gLoop;
        for(gLoop = 7; gLoop >= 0; gLoop--){
            x = g >> gLoop;
            if(x & 1){
                strcat(gBinary,"1");
            } else {
                strcat(gBinary,"0");
            }
        }

        //Convert the blue value to binary
        char * bBinary = malloc(8);
        bBinary[0] = '\0';
        char b = p->blue;
        int bLoop;
        for(bLoop = 7; bLoop >= 0; bLoop--){
            x = b >> bLoop;
            if(x & 1){
                strcat(bBinary,"1");
            } else {
                strcat(bBinary,"0");
            }
        }

        //Store the LSB of each colour as a binary string
        sprintf(binary + strlen(binary), "%c", rBinary[7]);
        sprintf(binary + strlen(binary), "%c", gBinary[7]);
        sprintf(binary + strlen(binary), "%c", bBinary[7]);
    }

    //For some reason there is sometimes a 9 at the start that shouldn't be there, I've tested it all out so I know to remove
    if(binary[0] = 9){
        memmove(&binary[0], &binary[0 + 1], strlen(binary) - 0);
    }
    printf("\n%s\n", binary);

    int j;
    for(j = 0; j < 800; j += 8){

        //Create an 8-bit string to convert into text
        char * unlock = malloc(8);
        sprintf(unlock, "%c", binary[j]);
        sprintf(unlock + strlen(unlock), "%c", binary[j + 1]);
        sprintf(unlock + strlen(unlock), "%c", binary[j + 2]);
        sprintf(unlock + strlen(unlock), "%c", binary[j + 3]);
        sprintf(unlock + strlen(unlock), "%c", binary[j + 4]);
        sprintf(unlock + strlen(unlock), "%c", binary[j + 5]);
        sprintf(unlock + strlen(unlock), "%c", binary[j + 6]);
        sprintf(unlock + strlen(unlock), "%c", binary[j + 7]);
        printf("\n%s   ", unlock);
        int ascii = binaryToInteger(atoi(unlock));
        printf("%c\n", ascii);
        sprintf(message, "%c", ascii);
    }
    printf("\n%s\n", message);
    return message;
}

int main(int argc, char * argv[]){

    //Verify there are the right number of arguments, exit if not
    if(argc != 2){
        printf("\nWrong number of arguments, format input as ./cw1.2 file.ppm\n");
        exit(0);
    }

    //Open the file, check it does not return NULL, then call getPPM function
    FILE * f = fopen(argv[1], "r");
    if(f == NULL){
        printf("Error opening file %s\n", argv[1]);
        exit(0);
    }
    struct PPM * im = getPPM(f);

    //Offer to show the PPM file before alterations
    char show[1];
    printf("\nWould you like to view the file before modification? Enter y or n\n");
    scanf("%s%*c", &show); //The %*c is to absorb the dropped line, otherwise it would skip over the next fgets()
    printf("\n"); //Formatting

    //If statement to react to user input
    if(strcmp(show, "y") == 0){
        showPPM(im);
    } else if(strcmp(show, "n") == 0){
        printf("File will not be shown\n");
    } else {
        printf("Input not recognised, will not show file\n");
    }

    //Option to encode or decode file
    printf("\nWould you like to encode or decode the image? Enter e or d\n");
    char option[1];
    scanf("%s%*c", &option);

    //React to user input
    if(strcmp(option, "e") == 0){

        char message[100];
        int secret;
        int mSize;

        //Message input
        printf("\nPlease enter the message you would like to hide in the image:\n");
        fgets(message, 100, stdin);
        message[strcspn(message, "\n")] = 0; //Remove the \n added to the end of message when enter is pressed
        if(strlen(message) > 100){
            printf("Message must not be more than 100 characters long");
            exit(0);
        }

        //Size of message
        mSize = strlen(message);
        printf("Your message is '%s' and the length of your message is %d\n", message, mSize);

        //Secret key used to hide message
        printf("\nPlease enter the secret key you would like to use (must be an integer):\n");
        scanf("%d", &secret);

        if(secret > 0){
            //Call encode method
            struct PPM * alt = encode(im, message, mSize, secret);
            //I'm pretty sure the information comes back from encode intact, but it is lost on the way to writeFile
            writeFile(alt);
        } else {
            printf("\nSecret key must be a positive integer\n");
        }

    } else if(strcmp(option, "d") == 0){
        
        int secret;

        printf("\nPlease enter the secret key you would like to use (must be an integer):\n");
        scanf("%d", &secret);

        if(secret > 0){
            //Call decode method
            char * message = decode(im, secret);
            printf("%s", message);
        } else {
            printf("\nSecret key must be a positive integer\n");
        }
    }
}